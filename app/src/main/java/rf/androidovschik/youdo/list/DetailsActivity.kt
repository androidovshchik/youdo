package rf.androidovschik.youdo.list

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_details.*
import rf.androidovschik.youdo.BaseActivity
import rf.androidovschik.youdo.Post
import rf.androidovshchik.youdo.R
import java.text.SimpleDateFormat

class DetailsActivity : BaseActivity() {

    // конвертирует unix время во что-то читаемое
    @SuppressLint("SimpleDateFormat")
    private val format = SimpleDateFormat("yyyy-MM-dd HH:mm")

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        // это определяет стрелку назад в toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val post = intent.getSerializableExtra("post") as Post
        // это определяет название в toolbar
        title = post.title
        category.text = "Категория: " + post.category
        price.text = "Стоимость: " + post.price + " руб"
        description.text = post.description
        address.text = "Адрес: " + post.address
        phone.text = "Телефон: " + post.phone
        timestamp.text = "Дата публикации: " + format.format(post.timestamp)
    }

    // false говорит, что не будет меню в toolbar
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // нажатие на стрелку назад в toolbar
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}