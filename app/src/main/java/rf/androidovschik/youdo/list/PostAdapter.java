package rf.androidovschik.youdo.list;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import durdinapps.rxfirebase2.RxFirestore;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import rf.androidovschik.youdo.Post;
import rf.androidovshchik.youdo.R;

public class PostAdapter extends FirestoreRecyclerAdapter<Post, PostAdapter.PostViewHolder> {

    private final boolean allTasks;

    public PostAdapter(FirestoreRecyclerOptions<Post> options, boolean allTasks) {
        super(options);
        this.allTasks = allTasks;
    }

    @Override
    @NonNull
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // здесь создается элементы списка
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post,
            parent, false));
    }

    @Override
    @SuppressLint("SetTextI18n")
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position, @NonNull Post post) {
        // здесь накладываются данные на каждый элемент списка
        holder.title.setText(post.getTitle());
        holder.category.setText(post.getCategory());
        holder.price.setText(post.getPrice() + " руб");
    }

    @Override
    public void onError(@NonNull FirebaseFirestoreException e) {
        e.printStackTrace();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {

        public TextView title;

        public TextView category;

        public TextView price;

        private ProgressDialog progressDialog;

        public PostViewHolder(@NonNull View view) {
            super(view);
            title = view.findViewById(R.id.title);
            category = view.findViewById(R.id.category);
            price = view.findViewById(R.id.price);
            if (allTasks) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // открываем детали записи
                        Context context = itemView.getContext().getApplicationContext();
                        Intent intent = new Intent(context, DetailsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        // post это модель записи
                        intent.putExtra("post", getItem(getAdapterPosition()));
                        context.startActivity(intent);
                    }
                });
            } else {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Context context = itemView.getContext();
                        // диалог подтверждения
                        new AlertDialog.Builder(context)
                            .setTitle("Удалить публикацию?")
                            .setNegativeButton(context.getString(android.R.string.cancel), null)
                            .setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @SuppressWarnings("ConstantConditions")
                                @SuppressLint("CheckResult")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    showProgressDialog();
                                    // здесь запись удаляется
                                    RxFirestore.deleteDocument(FirebaseFirestore.getInstance()
                                        .collection("posts")
                                        .document(getItem(getAdapterPosition()).getUid()))
                                        .subscribe(new Action() {
                                            @Override
                                            public void run() {
                                                closeProgressDialog();
                                            }
                                        }, new Consumer<Throwable>() {
                                            @Override
                                            public void accept(Throwable throwable) {
                                                throwable.printStackTrace();
                                                closeProgressDialog();
                                                Toast.makeText(context, "Не удалось удалить",
                                                    Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                }
                            })
                            .create()
                            .show();
                    }
                });
            }
        }

        @SuppressWarnings("AccessStaticViaInstance")
        private void showProgressDialog() {
            closeProgressDialog();
            final Context context = itemView.getContext();
            progressDialog = new ProgressDialog(context).show(context, "Пожалуйста, подождите...",
                null, true);
        }

        private void closeProgressDialog() {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }
}