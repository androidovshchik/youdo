package rf.androidovschik.youdo.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import rf.androidovschik.youdo.BaseActivity;
import rf.androidovschik.youdo.Post;
import rf.androidovschik.youdo.utils.DividerItemDecorator;
import rf.androidovshchik.youdo.R;

public class ListActivity extends BaseActivity {

    private PostAdapter adapter;

    @SuppressWarnings("FieldCanBeLocal")
    private boolean allTasks;

    @Override
    @SuppressWarnings("ConstantConditions")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        allTasks = getIntent().getBooleanExtra("all", false);
        // это определяет название в toolbar
        setTitle(allTasks ? "Все задания" : "Мои задания");
        // это определяет стрелку назад в toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        // это определяет ориентацию списка
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        // это определяет разделитель
        recyclerView.addItemDecoration(new DividerItemDecorator(ContextCompat.getDrawable(getApplicationContext(),
            R.drawable.divider)));
        // это определяет, что нужно загружать (какую коллекцию и с какими параметрами)
        FirestoreRecyclerOptions.Builder<Post> builder = new FirestoreRecyclerOptions.Builder<Post>();
        if (allTasks) {
            builder.setQuery(FirebaseFirestore.getInstance()
                .collection("posts")
                .orderBy("timestamp", Query.Direction.DESCENDING), Post.class);
        } else {
            builder.setQuery(FirebaseFirestore.getInstance()
                .collection("posts")
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .whereEqualTo("author", FirebaseAuth.getInstance().getCurrentUser().getUid()), Post.class);
        }
        adapter = new PostAdapter(builder.build(), allTasks);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // убираем часть меню
        menu.findItem(R.id.all_tasks).setVisible(false);
        menu.findItem(R.id.my_tasks).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // нажатие на стрелку назад в toolbar
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}