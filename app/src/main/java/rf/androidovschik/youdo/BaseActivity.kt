package rf.androidovschik.youdo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import rf.androidovschik.youdo.list.ListActivity
import rf.androidovshchik.youdo.R

/*
 Родитель активити для всех остальных
 самое главное здесь это
 - мониторинг авторизации (на каждом активити может выкинуть на логин)
 - обработка кликов меню
*/
abstract class BaseActivity : AppCompatActivity() {

    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    private val authListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
        if (firebaseAuth.currentUser == null) {
            // вот здесь так раз кидает на логин
            startActivityForResult(AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(listOf(AuthUI.IdpConfig.EmailBuilder().build()))
                .setTheme(R.style.AppTheme)
                .setTosUrl("https://example.com/terms-of-service.html")
                .setPrivacyPolicyUrl("https://example.com/privacy-policy.html")
                .build(), 1)
        }
    }

    public override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(authListener)
    }

    public override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(authListener)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // меню
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // здесь обрабатывается нажатия на меню
        when (item.itemId) {
            R.id.all_tasks -> {
                val intent = Intent(this@BaseActivity, ListActivity::class.java)
                // all определяет, что показывать (все или мои)
                intent.putExtra("all", true)
                startActivity(intent)
                return true
            }
            R.id.my_tasks -> {
                val intent = Intent(this@BaseActivity, ListActivity::class.java)
                intent.putExtra("all", false)
                startActivity(intent)
                return true
            }
            R.id.exit -> {
                auth.signOut()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}