package rf.androidovschik.youdo

import java.io.Serializable

// Это модель записи
class Post : Serializable {

    var uid: String? = null

    var category: String? = null

    var title: String? = null

    var price: String? = null

    var description: String? = null

    var address: String? = null

    var phone: String? = null

    var timestamp: Long = System.currentTimeMillis()

    var author: String? = null
}