package rf.androidovschik.youdo.utils;

import android.content.res.Resources;

public class ViewUtil {

    @SuppressWarnings("unused")
    public static int dp2px(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }
}
