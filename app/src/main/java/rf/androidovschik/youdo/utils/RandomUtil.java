package rf.androidovschik.youdo.utils;

import java.security.SecureRandom;

public class RandomUtil {

    //0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
    private static final String CHARS = "H2mNylkKVEsU5C6JrGhWa1pDA0ufRbewMgdctIXn8oBiZPjSxQ49YLOF3Tq7zv";

    private static final SecureRandom RANDOM = new SecureRandom();

    public static String generateUid() {
        StringBuilder stringBuilder = new StringBuilder(20);
        for (int i = 0; i < 20; i++) {
            stringBuilder.append(CHARS.charAt(RANDOM.nextInt(CHARS.length())));
        }
        return stringBuilder.toString();
    }
}
