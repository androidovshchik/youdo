package rf.androidovschik.youdo.publish

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_categories.*
import rf.androidovschik.youdo.BaseActivity
import rf.androidovshchik.youdo.R

class SubcategoryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        // это определяет стрелку назад в toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        // это определяет название в toolbar
        title = intent.getStringExtra("category")
        val array = resources.getStringArray(intent.getIntExtra("array", 0))
        for (item in array) {
            // это добавляет подкатегорию
            addItem(item)
        }
        // это добавляет категорию "Другое"
        other.setOnClickListener {
            val intent = Intent(applicationContext, PublishActivity::class.java)
            intent.putExtra("category", "Другое")
            startActivityForResult(intent, 2)
        }
    }

    private fun addItem(text: String) {
        val item = View.inflate(applicationContext, R.layout.item_subcategory, null)
        item.layoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        (item.findViewById(R.id.title) as TextView).text = text
        item.setOnClickListener {
            val intent = Intent(applicationContext, PublishActivity::class.java)
            intent.putExtra("category", text)
            startActivityForResult(intent, 2)
        }
        categories.addView(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        // убираем часть меню
        menu.findItem(R.id.all_tasks).isVisible = false
        menu.findItem(R.id.my_tasks).isVisible = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // нажатие на стрелку назад в toolbar
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                // тот случай, когда хотим вернуться на главную
                finish()
            }
        }
    }
}