package rf.androidovschik.youdo.publish

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.MenuItem
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import durdinapps.rxfirebase2.RxFirestore
import rf.androidovschik.youdo.BaseActivity
import rf.androidovschik.youdo.utils.RandomUtil
import rf.androidovschik.youdo.Post
import rf.androidovshchik.youdo.R
import android.text.method.DigitsKeyListener
import android.text.InputType
import android.view.Menu
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_publish.*

class PublishActivity : BaseActivity() {

    private var progressDialog: ProgressDialog? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publish)
        // это определяет стрелку назад в toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        // это определяет название в toolbar
        title = "Публикация"
        // этим допускаем в поле price только ввод чисел
        price.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        price.keyListener = DigitsKeyListener.getInstance("0123456789")
        category.text = "Категория: " + intent.getStringExtra("category")
        container.requestFocus()
        publish.setOnClickListener {
            // if проверяет, что все есть
            if (TextUtils.isEmpty(titleText.text)) {
                showMessage("Введите название")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(price.text)) {
                showMessage("Пустая стоимость")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(description.text)) {
                showMessage("Введите описание")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(address.text)) {
                showMessage("Введите адрес")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(phone.text)) {
                showMessage("Введите телефон")
                return@setOnClickListener
            }
            val post = Post()
            post.uid = RandomUtil.generateUid()
            post.category = intent.getStringExtra("category")
            post.title = titleText.text.toString()
            post.price = price.text.toString()
            post.address = address.text.toString()
            post.phone = phone.text.toString()
            post.description = description.text.toString()
            post.author = FirebaseAuth.getInstance().currentUser!!.uid
            showProgressDialog()
            // здесь идет добавление в бд
            RxFirestore.setDocument(FirebaseFirestore.getInstance()
                .collection("posts")
                .document(post.uid!!), post)
                .subscribe({
                    closeProgressDialog()
                    setResult(AppCompatActivity.RESULT_OK)
                    finish()
                }) { throwable ->
                    // здесь ловит ошибку при добавлении
                    closeProgressDialog()
                    if (throwable is FirebaseFirestoreException) {
                        showMessage(throwable.message!!)
                    } else {
                        showMessage("Не удалось опубликовать")
                    }
                }
        }
        cancel.setOnClickListener {
            // RESULT_OK говорит, что хотим вернутся на главную
            setResult(AppCompatActivity.RESULT_OK)
            finish()
        }
    }

    private fun showProgressDialog() {
        closeProgressDialog()
        progressDialog = ProgressDialog.show(this@PublishActivity, "Пожалуйста, подождите...",
            null, true)
    }

    private fun closeProgressDialog() {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
            progressDialog = null
        }
    }

    private fun showMessage(text: String) {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT)
            .show()
    }

    // false говорит, что не будет меню в toolbar
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // нажатие на стрелку назад в toolbar
        if (item.itemId == android.R.id.home) {
            // RESULT_CANCELED говорит, что хотим выбрать другую подкатегорию
            setResult(AppCompatActivity.RESULT_CANCELED)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(AppCompatActivity.RESULT_CANCELED)
        super.onBackPressed()
    }
}