package rf.androidovschik.youdo.publish;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ArrayRes;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import rf.androidovschik.youdo.BaseActivity;
import rf.androidovschik.youdo.utils.ViewUtil;
import rf.androidovshchik.youdo.R;

public class CategoriesActivity extends BaseActivity {

    private LinearLayout categories;

    private boolean doubleExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        categories = findViewById(R.id.categories);
        // это определяет название в toolbar
        setTitle("Категории");
        // это добавляет категорию
        addCategory(R.drawable.grid_stub_auto, R.array.grid_stub_auto, "Ремонт транспорта");
        addCategory(R.drawable.grid_stub_clerical, R.array.grid_stub_clerical, "Ремонт и строительство");
        addCategory(R.drawable.grid_stub_computer_help, R.array.grid_stub_computer_help, "Ремонт цифровой техники");
        addCategory(R.drawable.grid_stub_courier, R.array.grid_stub_courier, "Курьерские услуги");
        addCategory(R.drawable.grid_stub_design, R.array.grid_stub_design, "Дизайн");
        addCategory(R.drawable.grid_stub_electronic_repair, R.array.grid_stub_electronic_repair, "Компьютерная помощь");
        addCategory(R.drawable.grid_stub_health_and_beauty, R.array.grid_stub_health_and_beauty, "Красота и здоровье");
        addCategory(R.drawable.grid_stub_house, R.array.grid_stub_house, "Уборка и помощь по хозяйству");
        addCategory(R.drawable.grid_stub_legal_advice, R.array.grid_stub_legal_advice, "Юридическая помошь");
        addCategory(R.drawable.grid_stub_photoshop, R.array.grid_stub_photoshop, "Фото- и видео-услуги");
        addCategory(R.drawable.grid_stub_promo, R.array.grid_stub_promo, "Мероприятия и промо-акции");
        addCategory(R.drawable.grid_stub_teaching, R.array.grid_stub_teaching, "Репетиторы и образование");
        addCategory(R.drawable.grid_stub_tech_repair, R.array.grid_stub_tech_repair, "Установка и ремонт техники");
        addCategory(R.drawable.grid_stub_trucking, R.array.grid_stub_trucking, "Грузоперевозки");
        addCategory(R.drawable.grid_stub_virtual_assistant, R.array.grid_stub_virtual_assistant, "Виртуальный помощник");
        addCategory(R.drawable.grid_stub_web_development, R.array.grid_stub_web_development, "Web-разработка");
        // это добавляет категорию "Другое"
        findViewById(R.id.other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PublishActivity.class);
                intent.putExtra("category", "Другое");
                startActivity(intent);
            }
        });
    }

    private void addCategory(@DrawableRes int drawable, final @ArrayRes int array, final String text) {
        View category = View.inflate(getApplicationContext(), R.layout.item_category, null);
        category.setLayoutParams(new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            ViewUtil.dp2px(240)
        ));
        ((ImageView) category.findViewById(R.id.image)).setImageResource(drawable);
        ((TextView) category.findViewById(R.id.name)).setText(text);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SubcategoryActivity.class);
                intent.putExtra("category", text);
                intent.putExtra("array", array);
                startActivity(intent);
            }
        });
        categories.addView(category);
    }

    @Override
    public void onBackPressed() {
        // этот код принуждает два раза нажать назад для выхода из приложения
        if (doubleExit) {
            super.onBackPressed();
            return;
        }
        doubleExit = true;
        Toast.makeText(this, "Нажмите еще раз, чтобы выйти", Toast.LENGTH_SHORT)
            .show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleExit = false;
            }
        }, 2000);
    }
}